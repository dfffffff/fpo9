#########################################################################
#
# Copyright (C) 2018,2021 David Flamand
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
#########################################################################

.PHONY: all clean distclean check
.SUFFIXES:

AS = as09
NAME = float09
SRCDIR = src
TMPDIR = tmp
SOURCE = $(NAME).a09
CONVERT = tools/convert.sh
SRECORD = tools/srecord.sh

ifeq ($(OPT),)
ASFLAGS = -q -O0 -f m
S19 = $(NAME).s19
BIN = $(NAME).bin
LIST = $(NAME).l
CHECKSUM=c5332caac4cda960d240b059b6fccc9f450140ce94a0ebf5893c1f8f0fb83813
else
ifeq ($(OPT),1)
ASFLAGS = -q -O1 -f m -B -D NOXREF -D NOPAD
S19 = $(NAME)_opt.s19
BIN = $(NAME)_opt.bin
LIST = $(NAME)_opt.l
CHECKSUM=a9506b1c1e70f1546c8253ee1f067069e539978cbd4e84b21f2e6aec7dfdb652
else
$(error OPT value unsupported)
endif
endif

HDR := \
	$(SRCDIR)/EQUATES.SA \
	$(SRCDIR)/MACROS.SA

SRC := \
	$(SRCDIR)/FRNBAK.SA \
	$(SRCDIR)/GETPUT.SA \
	$(SRCDIR)/PROCS.SA \
	$(SRCDIR)/CHECK.SA \
	$(SRCDIR)/NOTRAP.SA \
	$(SRCDIR)/RNDEXEP.SA \
	$(SRCDIR)/OUTS.SA \
	$(SRCDIR)/INTFLT.SA \
	$(SRCDIR)/COMP.SA \
	$(SRCDIR)/COMPARE.SA \
	$(SRCDIR)/DISPAT.SA \
	$(SRCDIR)/MVABSNEG.SA \
	$(SRCDIR)/FMULDV.SA \
	$(SRCDIR)/FRMSQT.SA \
	$(SRCDIR)/INS.SA \
	$(SRCDIR)/UTILIO.SA \
	$(SRCDIR)/FADS.SA \
	$(SRCDIR)/UTIL.SA \
	$(SRCDIR)/ENDIT.SA

HDR_I09 := $(subst .SA,.I09,$(subst $(SRCDIR)/,$(TMPDIR)/,$(HDR)))
SRC_A09 := $(subst .SA,.A09,$(subst $(SRCDIR)/,$(TMPDIR)/,$(SRC)))

all: $(BIN)

clean:
	rm -f -- $(HDR_I09) $(SRC_A09) $(LIST)
	[ -d $(TMPDIR) ] && rmdir $(TMPDIR) || printf ""

distclean: clean
	rm -f -- $(S19) $(BIN)

$(TMPDIR):
	mkdir $@

$(HDR_I09): $(HDR) | $(TMPDIR)
	$(CONVERT) <$(subst .I09,.SA,$(subst $(TMPDIR)/,$(SRCDIR)/,$@)) >$@

$(SRC_A09): $(SRC) | $(TMPDIR)
	$(CONVERT) <$(subst .A09,.SA,$(subst $(TMPDIR)/,$(SRCDIR)/,$@)) >$@

$(S19): $(SOURCE) $(HDR_I09) $(SRC_A09) | $(TMPDIR)
	$(AS) $(ASFLAGS) -I $(TMPDIR) -o $@ -l $(LIST) $(SOURCE)

$(BIN): $(S19)
	$(SRECORD) -crc <$< >$@

check: $(BIN)
	@{ \
		set -e ;\
		printf "checksum of $<: " ;\
		param1() { printf %s $$1; } ;\
		CSUM=$$(param1 $$(sha256sum $<)) ;\
		if [ "_$$CSUM" = "_$(CHECKSUM)" ]; then \
			printf "GOOD\n"; exit 0 ;\
		else \
			printf "WRONG\n"; exit 1 ;\
		fi \
	}

# EOF
