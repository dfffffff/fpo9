#!/bin/sh
#########################################################################
#
# Copyright (C) 2018,2021 David Flamand
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
#########################################################################
#
# This shell script does the following:
#  - Convert Motorola .SA format to as09 format.
#  - Read from the stdin and write to the stdout.
#  - Patch the code for text error.
#  - Convert tab to space. (cosmetic)
#  - Align end of line comments. (cosmetic)
#
#########################################################################

set -e
export LC_ALL=C

# non-inherent directives
DINI="EQU|FCB|FDB|IFEQ|IFGE|IFGT|IFLE|IFLT|IFNE|TTL"
# inherent directives
DIIN="ENDC|ENDM|PAGE"
# branch opcodes
BR="[L]?B[CV]{1,1}[CS]{1,1}|[L]?BEQ|[L]?BG[ET]{1,1}|[L]?BH[IS]{1,1}|[L]?BL[EOST]{1,1}|[L]?BMI|[L]?BNE|[L]?BPL|[L]?BR[AN]{1,1}"
# non-inherent opcodes
OPNI="ADC[AB]{1,1}|ADD[ABD]{1,1}|AND[AB]{1,1}|ANDCC|[AL]{1,1}S[LR]{1,1}|[L]?BSR|BIT[AB]{1,1}|CLR|CMP[ABDSUXY]{1,1}|COM|CWAI|DEC"
OPNI="$OPNI|[E]?OR[AB]{1,1}|EXG|INC|JMP|JSR|LD[ABDSUXY]{1,1}|LEA[SUXY]{1,1}|NEG|ORCC|PSH[SU]{1,1}|PUL[SU]{1,1}|RO[LR]{1,1}"
OPNI="$OPNI|SBC[AB]{1,1}|ST[ABDSUXY]{1,1}|SUB[ABD]{1,1}|TFR|TST[AB]{1,1}"
# inherent opcodes
OPIN="ABX|[AL]{1,1}S[LR]{1,1}[AB]{1,1}|CLR[AB]{1,1}|COM[AB]{1,1}|DAA|DEC[AB]{1,1}|INC[AB]{1,1}|MUL|NEG[AB]{1,1}|NOP"
OPIN="$OPIN|RO[LR]{1,1}[AB]{1,1}|RT[IS]{1,1}|SEX|SWI[23]?|SYNC|TST[AB]{1,1}"
# inherent macros
MCIN="DECD|ENDIF|RIGHT1|SEC|XPLUSY|XSBTRY"
# inherent non-inherent macros
MCBT="ELSE|ENDWH"

HX="\\\$"
DP=";@"
CMT="; "
SPC="[ 	]+"
NSP="[^ 	]+"
SPCOPT="[ 	]*"
NSPOPT="[^ 	]*"
MNE="[^;_#@& 	]+"
LBL="[_0-9A-Z]*"
LBL2="[\$_0-9A-Z]+"
NINH="$OPNI|$BR|$DINI"
INHE="$OPIN|$DIIN|$MCIN"
BEG="$LBL$SPC|\\\\\\.[0-9A]{1,1}$SPC"
END="$SPCOPT\$|$SPC$NSP"
END2="$SPCOPT\$|$SPC"
END3="$SPCOPT\$|$SPC.*$"
EOL="$(printf '\15|\32')"

# convert tabs to spaces #\
expand \
	| \
# convert and patch the source code #\
sed -r \
	-e "s%($EOL)\$%%" \
	-e "s%^($BEG)($OPNI)($SPC)[-+]?[\$]?[0]+(,[XYUS]{1,1})($END)%\1\2\3\4\5%" \
	-e "s%^($BEG)(IF|MOVD|WHILE)($SPC)($NSPOPT\\()[-+]?[\$]?[0]+(,[XYUS]{1,1})(\\)$NSPOPT)($END)%\1\2\3\4\5\6\7%" \
	-e "s%^($BEG)(FCC)($SPC)(/[^/]*/)($END)%\1_\2\3\4\5%" \
	-e "s%^($BEG)(TTL)($END)%\1@\2\3%" \
	-e "s%^($BEG)(FAIL)($END)%\1\&\2\3%" \
	-e "s/^($BEG)($MCBT)($SPC)(L)($END2)/\1\`\2\3\4\5/" \
	-e "s/^($BEG)(ELSE)($SPC)\.($END)/\1\2\3\4/" \
	-e "s/^($BEG)($INHE|$MCBT)($SPC$NSP)/\1#\2\3/" \
	-e "s/^($BEG)\`($MCBT)($END2)/\1\2\3/" \
	-e "s/^($BEG)($MNE)($SPC$NSP$SPC)($NSP)/\1\2\3$CMT\4/" \
	-e "s/^($BEG)#($MNE)($SPC)($NSP)/\1\2\3$CMT\4/" \
	-e "s%^($BEG)_($MNE)($SPC)(/[^/]*/)($SPC)($NSP)%\1\2\3\4\5$CMT\6%" \
	-e "s%^($BEG)_($MNE)($SPC)(/[^/]*/)($SPCOPT)%\1\2\3\4\5%" \
	-e "s%^($BEG)@($MNE)($END)%\1\2\3%" \
	-e "s%^($SPC)&FAIL($SPC)(.*)%$DP\1ERR\2\3%" \
	-e "s%^\*.*%$DP&%" \
	-e "s%^@%%" \
	-e "s%NARG%\\\\#%g" \
	-e "s%^($BEG)L(SRD)($END)%\1\2 \3%" \
	-e "s%(${HX}A5)!X(\(\(NAME-ROMSTR\))!>(8\))!X(\(\(NAME-ROMSTR\))!\.(${HX}FF\))%\1^\2>\3^\4\&\5%" \
	-e "s%(#| \()(\(-\(\\\0\)-1\))!\.(${HX}FF)%#\2\&\3%" \
	-e "s%^($BEG)(ROL)$SPC(B)($END2)%\1\2\3\4%" \
	-e "s%^(  )( +)(IFCC NE)([*]+)$%\1\2\3\n\2\2LBSR\1SUBIAS\n\1\2ELSE\n\2\2LBSR\1OVFLNT\n\1\2ENDIF\n\2ENDIF\n\1ENDIF\n\1RTS\n;*\n;\4\4%" \
	-e "s% +RDERED PROPER$%%" \
	-e "s%$SPC($CMT)%\1%" | \
# align end of line comments #\
awk "	substr(\$0,1,1)!=\";\" && (i=index(\$0,\"$CMT\"))>0 {
		printf \"%-29s ; %s\n\",substr(\$0,1,i-1),substr(\$0,i+length(\"$CMT\"))
		next
	}
	{
		print
	}" | \
# remove "don't process" marker #\
sed "s/^$DP//"

# EOF
