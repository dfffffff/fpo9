#!/bin/sh
#########################################################################
#
# Copyright (C) 2021 David Flamand
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#########################################################################
#
# S19 to binary converter, implemented in awk.
#  - Create a flattened image from overlapped data,
#    only the last instance is kept.
#  - Will output bytes from the first defined data address to the last,
#    padded with zeros if any gap between.
#  - Will halt on any error including bad checksum.
#  - Read from the stdin and write to the stdout.
#  - Optionally an OS-9 CRC is appended (option -crc).
#
#########################################################################

set -e
export LC_ALL=C

TEST="BEGIN { xor(0, 0) }"
if awk "$TEST" <>/dev/null >&0 2>&0; then
	AWK="awk"
elif gawk "$TEST" <>/dev/null >&0 2>&0; then
	AWK="gawk"
elif busybox awk "$TEST" <>/dev/null >&0 2>&0; then
	AWK="busybox awk"
else
	printf "no suitable awk version found\n" >&2
	exit 1
fi

$AWK -v opt="$*" '

function hi(x)
{
	return int(x / 256)
}

function lo(x)
{
	return x % 256
}

function err()
{
	print "malformed s-record" > "/dev/stderr"
	exit 2
}

function chk()
{
	print "checksum mismatch" > "/dev/stderr"
	exit 3
}

function hex(t)
{
	x = 0
	for (y=1; y<=length(t); y++) {
		z = h[substr(t, y, 1)]
		if (z == "")
			err()
		x = x * 16 + z
	}
	return x
}

BEGIN {
	for(i=0; i<16; i++)
		h[sprintf("%X", i)] = i
	b = 65535
	c = 0
	e = 0
}

END {
	if (opt != "-crc") {
		for (i=b; i<=c; i++)
			printf "%o\n", m[i]+0
		exit 0
	}
	u0 = 255
	u1 = 255
	u2 = 255
	for (i=b; i<=c; i++) {
		v = m[i]+0
		printf "%o\n", v
		s = xor(v, u0)
		u0 = u1
		u1 = u2
		t = s * 2
		u1 = xor(u1, hi(t))
		u2 = lo(t)
		t = s * 64
		u1 = xor(u1, hi(t))
		u2 = xor(u2, lo(t))
		s = xor(s, lo(s * 2))
		s = xor(s, lo(s * 4))
		s = xor(s, lo(s * 16))
		if (s > 127) {
			u0 = xor(u0, 128)
			u2 = xor(u2, 33)
		}
	}
	printf "%o\n%o\n%o\n", 255-u0, 255-u1, 255-u2
}

{
	if (e != 0) {
		if ($0 == sprintf("%c", 26))
			exit 0
		err()
	}
	t = substr($0, 1, 2)
	l = substr($0, 3, 2)
	a = substr($0, 5, 4)
	if (length(t) != 2 || length(l) != 2 || length(a) != 4)
		err()
	l = hex(l)
	a = hex(a)
	if (l < 3)
		err()
	if (t == "S1" || t == "S9" || t == "S0") {
		s = l + int(a / 256) + (a % 256)
		l = (l - 3) * 2 + 9
		for (i=9; i<l; i+=2) {
			v = substr($0, i, 2)
			if (length(v) != 2)
				err()
			v = hex(v)
			s += v
			if (t == "S1") {
				m[a] = v
				if (a < b)
					b = a
				if (a > c)
					c = a
				a++
			}
		}
		v = substr($0, i, 2)
		if (length(v) != 2)
			err()
		if (hex(v) != (255 - (s % 256)))
			chk()
		v = substr($0, i + 2, 2)
		if (v != "" && v != "\r")
			err()
		if (t == "S9")
			e = 1
	}
	else
		err()
}

' | { IFS=""; while read -r l; do printf "\\$l"; done }

# EOF
