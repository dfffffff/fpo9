MC6809 Floating Point Package also known as MC6839
==================================================

The floating Point Package was originally intended to be a ROM chip (MC6839) containing [IEEE 754](https://en.wikipedia.org/wiki/IEEE_754) routines for the [MC6809](https://en.wikipedia.org/wiki/Motorola_6809).

Motorola later published the source code of the firmware on its public ftp.

This project contain the original archive file, and some script to convert Motorola assembly format to as09 format, it also generate a binary from the source code. The generated binary and can be loaded anywhere in the 6809 memory, it is also a valid OS-9 module.

The archive containing the source code was downloaded from Motorola public ftp in 1998.

---

### Caveats

* The source code provided by Motorola is older than the binary available on the internet, 1980 for the source code and 1982 for the binary.
* One of the source file is corrupted (text deleted), I was able to recover the missing source code part from the newer binary, the converter automatically patch the code when building.
* To date it is unknown to what extent the implementation differs from the IEEE 754-1985 standard.

---

### Build requirements

* A Unix-like machine with common tools:
  * /bin/sh [ awk expand make printf rm rmdir sed sha256sum
  * A compatible awk version: gawk or busybox awk (must support xor function)
* [as09 cross assembler](https://gitlab.com/dfffffff/as09), powerful assembler supporting macro and conditional assembly:
  * At least version 1.4.0
  * Must be accessible from PATH environment variable

---

### How to build

#### The original version

Building the 1982 version (byte-perfect):
* Outputs a ROMable binary, 8192 bytes in size
* The binary is compatible with OS-9
* Standard two pass assembler mode

```sh
make all check
```

#### The optimized version

Building the 1982 version with all optimizations enabled:
* Outputs a ROMable binary, 8170 bytes in size
* The binary is compatible with OS-9
* OS-9 Module revision level changed to 3
* Multi-pass assembler mode
* The assembler process the source files as it is a single file
* Operand size trimmed down if permitted
* Long branch converted to short branch if permitted

```sh
make all check OPT=1
```

---

### License

#### All the files excluding Motorola source files and archive

Copyright (C) 2018,2021 David Flamand

This program is distributed in the hope that it will be useful,\
but WITHOUT ANY WARRANTY; without even the implied warranty of\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

#### Motorola source files and archive

COPYRIGHT (C) MOTOROLA 1980-1982

---

### Contact

A lot of work was made to make this happen, Please give credit appropriately.

GitLab project page <https://gitlab.com/dfffffff/fpo9>

David Flamand <dflamand@gmail.com>

